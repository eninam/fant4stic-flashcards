import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { FlashCardsRoutes } from '../pages/flashcards/routes/FlashCardsRoutes'

export const AppRouter = () => {
  return (
    <Routes>
        <Route path='/*' element={ <FlashCardsRoutes/>}/>
    </Routes>
  )
}
