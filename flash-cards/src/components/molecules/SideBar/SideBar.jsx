import { getLocalDefaultSubjects } from '../../../Helpers/loadDefaultJSON';
import { ButtonSideBar } from '../../atoms/ButtonSideBar/ButtonSideBar'
import './SideBar.css'

export const SideBar = () => {
    const subjects = getLocalDefaultSubjects();
    return (
    <div className="navigation">
        <ul>
            {subjects.map((subject, index) => (
                <SideBarButton 
                    key={index} 
                    id={subject.id} 
                    text={subject.name} 
                    icon={"apps"}>
                </SideBarButton>  
            ))}
        </ul>
    </div>
    )
}


