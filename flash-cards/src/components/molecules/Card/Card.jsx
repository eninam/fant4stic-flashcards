import { useImmer } from 'use-immer';
import { CardButton } from '../../atoms/CardButton/CardButton';
import { Text } from '../../atoms/Text/Text';
import { Title } from '../../atoms/Title/Title';
import { ModalCard } from '../../organisms/ModalCard/ModalCard';
import { RegisterTopic } from '../../organisms/RegisterTopic/RegisterTopic';
import './Card.css'

export const Card = ({topic}) => {
    const [state, setState] = useImmer({ open: false });

    const handleOpen = () => setState(state => { state.open = true });
    const handleClose = () => setState(state => { state.open = false });
    return (
        <>
            <div className="card">
                <div className="box">
                    <div className="content">
                        
                    <Title 
                        variant={"h6"} 
                        text={topic.name}>
                    </Title>
                    <Text 
                        text={topic.description} 
                        variant={'p'}>
                    </Text>
                        <CardButton 
                            text={"See Card"} 
                            color={"primary"} 
                            variant={"contained"} 
                            onClickButton={handleOpen}>
                        </CardButton>
                    </div>
                </div>
            </div>
            <ModalCard open={state.open} topic={topic} handleClose={handleClose}></ModalCard>
        </>
    )
}
