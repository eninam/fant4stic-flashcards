import { Link } from 'react-router-dom'
import './SideBarButton.css'

export const SideBarButton = ({id, text, icon}) => {
  return (
    <li>
        <Link to={`/subjects/${ id }`}>
            
                <span className="icon"><ion-icon name={icon}></ion-icon></span>
                <span className="title">
                    {text}           
                </span>
            

        </Link>
    </li>
  )
}
