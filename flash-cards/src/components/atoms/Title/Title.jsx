import { Typography } from "@mui/material"

export const Title = ({text, variant}) => {
  return (
    <Typography variant={variant}>
        {text}
    </Typography>
  )
}
