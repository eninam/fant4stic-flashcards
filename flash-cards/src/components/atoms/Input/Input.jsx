import { Grid, TextField } from '@mui/material'
import React from 'react'

export const Input = ({label, type, placeholder, name, value, handleTextChange}) => {
  return (
    <>
        <Grid item xs={ 12 } sx={{ mt: 2 }}>
            <TextField 
            label={label}
            type={type} 
            placeholder={placeholder}
            name={name}
            value={value}
            onChange={handleTextChange}
            fullWidth
            />
        </Grid>
    </>
  )
}
