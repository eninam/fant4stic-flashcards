import { Typography } from "@mui/material"

export const Text = ({text, variant}) => {
  return (
    <Typography variant={variant}>
        {text}
    </Typography>
  )
}
