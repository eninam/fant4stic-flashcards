import { Button } from "@mui/material"
import SendIcon from '@mui/icons-material/Send';

export const CardButton = ({text, variant, color, icon=<SendIcon/>, onClickButton, type}) => {
  return (
    <>
    <Button variant={variant} 
            color={color}
            endIcon={icon}
            onClick={onClickButton}
            type={type}>
        { text }
    </Button>
    </>
  )
}
