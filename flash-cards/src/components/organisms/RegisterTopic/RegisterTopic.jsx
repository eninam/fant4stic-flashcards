import { Grid, TextField } from '@mui/material'
import {useImmer} from 'use-immer';
import { useForm } from '../../../Helpers/hooks/useForm';
import { CardButton } from '../../atoms/CardButton/CardButton'
import { Input } from '../../atoms/Input/Input'
import { Title } from '../../atoms/Title/Title'
import { ModalGeneric } from '../../molecules/Modal/ModalGeneric'
import { persistence } from '../../../Helpers/persistence/localStorage';

export const RegisterTopic = ({idSubject, open, handleClose, onNewTopic}) => {
 
  
    const { name, description, onInputChange, onResetForm } = useForm({
        name: '',
        description: ''
    });

    const key = 'topics';
    const onFormSubmit = (event) => {
        event.preventDefault();
        const newTopic = {
            id: new Date().getTime(),
            name,
            description,
            subjectId: idSubject
        }
        persistence.UpdateItems(key, newTopic )
        onNewTopic(newTopic);
        onResetForm();
    }

    return (
        <>
            <ModalGeneric open={open} handleClose={handleClose}>
                <form onSubmit={onFormSubmit}>
                    <Grid container>
                        <Title 
                            text={"Register a new topic"} 
                            variant={'h5'}>
                        </Title>
                        
                        <Grid item xs={ 12 } sx={{ mt: 2 }}>
                            <TextField 
                                label="Card title" 
                                type="text" 
                                placeholder='Enter the title'
                                name="name"
                                value={name}
                                onChange={onInputChange}
                            fullWidth
                            />
                        </Grid>
                        
                        <Grid item xs={ 12 } sx={{ mt: 2 }}>
                            <TextField 
                                label="Card description" 
                                type="text" 
                                placeholder='Enter the title'
                                name="description"
                                value={description}
                                onChange={onInputChange}
                            fullWidth
                            />
                        </Grid>                  
                    </Grid>
                    <CardButton 
                        text={"Add"} 
                        color={"primary"} 
                        variant={"contained"} 
                        type={"submit"}>
                            
                    </CardButton>
                </form>
            </ModalGeneric>

        </>
    )
}
