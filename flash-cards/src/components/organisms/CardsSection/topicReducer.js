
export const topicReducer = ( initialState = [], action ) => {


    switch ( action.type ) {
        case 'Add Topic':
            return [ ...initialState, action.payload ];

        case 'Remove Topic':
            return initialState.filter( topic => topic.id !== action.payload );

        default:
            return initialState;
    }
}