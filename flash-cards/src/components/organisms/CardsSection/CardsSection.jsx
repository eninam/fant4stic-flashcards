import { useParams } from "react-router-dom";
//import { getTopicBySubject } from "../../../Helpers/loadDefaultJSON";
import FloatButton from "../../atoms/FloatButton/ActionButtons";
import { Card } from "../../molecules/Card/Card";
import { ModalCard } from "../ModalCard/ModalCard";
import { RegisterTopic } from "../RegisterTopic/RegisterTopic";
import { useImmer } from 'use-immer';
import { persistence } from "../../../Helpers/persistence/localStorage";
import { storageUtils } from "../../../Helpers/persistence/storageUtils";
import { getLocalDefaultSubjects, getLocalDefaultTopics } from "../../../Helpers/loadDefaultJSON";

import "./CardsSection.css"

export const CardsSection = () => {
    const [state, setState] = useImmer({ open: false });

    const handleOpen = () => setState(state => { state.open = true });
    const handleClose = () => setState(state => { state.open = false });

    const handleNewTopic = (topic) => {
        console.log({topic})
    }
    const key = 'topics';
    const keySubject = 'subjects';
    const {id: subjectId} = useParams();
    persistence.loadItems(key, getLocalDefaultTopics())
    persistence.loadItems(keySubject, getLocalDefaultSubjects())
    const topics =  storageUtils.getItemsLocalStorageId(key, subjectId);
    
    return (
        <div className="main">
            <FloatButton text={"Add a new topic"} open={handleOpen}></FloatButton>
            <div className="content-cards">
                {topics.map((topic) => (
                        <Card key={topic.id} topic= {topic}></Card>
                    ))
                }
            </div>
            <RegisterTopic 
                idSubject={subjectId} 
                open={state.open} 
                handleClose={handleClose}
                onNewTopic={handleNewTopic}></RegisterTopic>
        </div>
    )
}
