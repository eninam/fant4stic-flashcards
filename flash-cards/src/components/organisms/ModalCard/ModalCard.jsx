import React from 'react'
import { Text } from '../../atoms/Text/Text'
import { Title } from '../../atoms/Title/Title'
import { ModalGeneric } from '../../molecules/Modal/ModalGeneric'

export const ModalCard = ({open, handleClose, topic}) => {
  return (
    <>
        <ModalGeneric open={open} handleClose={handleClose}>
            
            <Title 
                text={topic.name} 
                variant={'h5'}>
            </Title>
            
            <Text 
                text={topic.description} 
                variant={'p'}>
            </Text>
                
        </ModalGeneric>
    </>
  )
}
