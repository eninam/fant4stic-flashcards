
import { SideBar } from '../../molecules/SideBar/SideBar';
import { CardsSection } from '../../organisms/CardsSection/CardsSection';


export const FlashCardsLayout = ({ children }) => {
    
  return (
    <>
      <div className="container">
        <SideBar/>
        <CardsSection/>      
      </div>
      
    </>

   
  )
}
