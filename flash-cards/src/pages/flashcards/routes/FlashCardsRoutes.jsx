
import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import { HomePage } from '../HomePage'

export const FlashCardsRoutes = () => {
  return (
    <Routes>
        <Route path='/' element={<HomePage/>}/>
        <Route path='/subjects/:id' element={<HomePage/>}/>
        <Route path='/*' element={<Navigate to="/"/>}/>
    </Routes>
  )
}
