import { useImmer } from 'use-immer';

export const useForm = ( initialForm = {} ) => {
  
    const [ formState, setFormState ] = useImmer( initialForm );

    const onInputChange = ({ target }) => {
        const { name, value } = target;
        setFormState({
            ...formState,
            [ name ]: value
        });
    }

    const onResetForm = () => {
        setFormState( initialForm );
    }

    return {
        ...formState,
        formState,
        onInputChange,
        onResetForm,
    }
}
