import { getItemsLocalStorage} from "./LocalStorage";
import { getItemsLocalStorageId } from "./getItemsLocalStorageId";

export const storageUtils = {
    getItemsLocalStorageId: (key, id) => getItemsLocalStorageId( getItemsLocalStorage(key), id)
}
