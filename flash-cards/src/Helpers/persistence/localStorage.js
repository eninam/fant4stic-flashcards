export const getItemsLocalStorage = (key) => 
{
    const items = localStorage.getItem(key);
    return items ? JSON.parse(items) : [];
}

export const saveItemsLocalStorage = (key, items) => 
{
    localStorage.setItem(key, JSON.stringify(items));
}

function loadItems(key, initialItems) {
    let items = getItemsLocalStorage(key);
    if(items.length === 0) {
        items = initialItems;
        saveItemsLocalStorage(key, items);
    }

    return new Promise((resolve, reject) => {
        if (items.length === 0) {
            reject(new Error('Empty values.'))
        }
        setTimeout(() => {
            resolve(items);
        }, 1500);
    })
}

function UpdateItem(key, newItem){
    let items = JSON.parse(localStorage.getItem(key)) || [];
    items.push(newItem);
    localStorage.setItem(key, JSON.stringify(items));
}

export const persistence = {
    getItemsLocalStorage: (key) => getItemsLocalStorage,
    saveItemsLocalStorage: (key, items) => saveItemsLocalStorage(key, items),
    loadItems: (key, initialItems) => loadItems(key, initialItems),
    UpdateItems: (key, newItem) => UpdateItem(key, newItem)
}
