function loadItemsAPI(key, initialItems) {
    return fetch(`https://localhost/${key}`)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
         console.log(response);
          throw new Error('Network response was not ok');
        }
      })
      .then(data => {
        if (data.length === 0) {
          data = initialItems;
          saveItemsLocalStorage(key, data);
        }
        return new Promise(resolve => {
          setTimeout(() => {
            resolve(data);
          }, 1500);
        });
      })
      .catch(error => {
        console.error('There was a problem fetching the items:', error);
      });
  }

function getItemByIdAPI(key, id) {
    return fetch(`https://localhost/${key}/${id}`)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Network response was not ok');
        }
      })
      .catch(error => {
        console.error('There was a problem fetching the item:', error);
      });
  }

  function createItemAPI(key, newItem) {
    return fetch(`https://localhost/${key}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newItem)
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Network response was not ok');
        }
      })
      .catch(error => {
        console.error('There was a problem creating the item:', error);
      });
  }


  function updateItemAPI(key, newItem) {
    return fetch(`https://localhost/${key}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(newItem)
    })
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
      })
      .catch(error => {
        console.error('There was a problem updating the item:', error);
      });
  }
  

const testCard = {
    "title": "How to ",
    "description": ""
}

const testTopic = {
    "name": "Entity Framework Core",
    "description": ""

}

  export const persistence = {
    loadItemsAPI: (key, initialItems) => loadItemsAPI(key, initialItems),
    updateItemAPI: (key, newItem) => updateItemAPI(key, newItem),
    getItemByIdAPI: (key, id) => getItemByIdAPI(key, id),
    createItemAPI: (key, newItem) => createItemAPI(key, newItem)
  };


