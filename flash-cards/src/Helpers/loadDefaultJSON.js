import jsonSubjectsData from '../../data/subjects.json';
import jsonTopicsData from '../../data/topic.json';

export function getLocalDefaultSubjects() {
  return jsonSubjectsData;
}

export function getLocalDefaultTopics() {
  return jsonTopicsData;
}